#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 20 21:07:27 2018

@author: Miłosz Martynow
"""

import make_inputs as MI

MI = MI.MakeInputs('graphene.in', '.txt')
inputs = MI.find_inputs()

for file in inputs:
    
    input_scheme = MI.open_file()
    file_without_extension = file[:-4]
    
    ### INSER PREFIX LINE
    # find number of line with PREFIX
    line_number = MI.find_keyword('PREFIX_keyword\n')
    # make two lists with before and after PREFIX line
    before, after = MI.divide_file(line_number = line_number)
    # make new PREFIX line
    prefix_line = ['prefix = \"' + file_without_extension + '\",\n']
    # update the input_scheme by inserting it between before and after list
    input_scheme = MI.list_combine(before, prefix_line, after)

    ### INSERT GAP LINE FROM FILES
    # find number of line with PREFIX
    line_number = MI.find_keyword('GAP_keyword\n')
    # make two lists with before and after GAP line
    before, after = MI.divide_file(filename = input_scheme, line_number = line_number)
    # make new GAP lines
    insert_file = MI.open_file(filename = file)
    insert_file[-1] = insert_file[-1]+'\n'
    # update the input_scheme by inserting the GAP lines
    input_scheme = MI.list_combine(before, insert_file, after)

    ### SAVE OUTPUTS
    filename = file_without_extension + '.in'
    MI.save_file(inside_of_file = input_scheme, filename = filename)