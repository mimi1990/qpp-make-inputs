#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 21 08:02:22 2018

@author: Miłosz Martynow
"""

import numpy as np
import matplotlib.pyplot as plt
plt.close()

data = np.loadtxt('graphene_gap_A.txt')

x_A  = data[:,0]
y_Ht = data[:,1]
y_eV = data[:,2]

minimum = x_A[np.argmin(y_Ht)]

fig, ay1 = plt.subplots() 

ay1.plot(x_A, y_Ht,marker='.', label='Total energy')
ay1.set_ylabel("Total energy [Ht]")
ay1.set_xlabel("Graphene sheet distance [\u212B]")
ay1.axvline(minimum,color='k',alpha=0.6,linewidth = 0.2)

ay2 = ay1.twinx()
ay2.plot(x_A, y_eV, 'r+', linestyle = ':', label = 'Fermi energy')
ay2.set_ylabel("Fermi energy [eV]")
ay2.axhline(0,color='k',alpha=0.6,linewidth = 0.2)

leg = fig.legend(loc=9)
leg.get_frame().set_alpha(1)


fig.tight_layout()
plt.savefig('TE_FE_sheet.png', bbox_inches='tight', format='png', dpi=1000)