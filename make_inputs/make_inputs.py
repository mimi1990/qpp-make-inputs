#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 18 17:22:13 2018

@author: foton
"""
import os

class MakeInputs:
    
    class_calls = 0
    
    def __init__(self, basis_filename, extension):
        
        self.basis_filename = basis_filename
        self.extension = extension
        MakeInputs.class_calls += 1
    
    def find_inputs(self, ext = None):
        ### Find files ending with specific extension
        ### Default is the one from __init__
        ### For some purposes you can set differend extention in object
        
        if ext == None:
            ext = self.extension
        else:
            ext = ext
        
        ls = []
        #work_dir = os.getcwd() + '/'
        for line in os.listdir(os.getcwd()):
            if line.endswith(ext):
                ls.append(line)
            
        return ls
    
    def open_file(self, filename = None):
        
        if filename == None:
            file = self.basis_filename
        else:
            file = filename
            
        try:
            file_list = []
            with open(file, 'r') as file:
                for line in file:
                    file_list.append(line)
        except FileNotFoundError:
            print('File nope.')
        
        return file_list
    
    def save_file(self, inside_of_file = None, filename = None):
        
        if filename == None:
            file = self.basis_filename
        else:
            file = filename
            
        if inside_of_file == None:
            inside_of_file = self.open_file()
        else:
            inside_of_file = inside_of_file    
            
        with open(file, 'w') as file:
            for line in inside_of_file:
                file.write('{}'.format(line))
            
    def find_keyword(self, keyword, filename = None):
        
        if filename == None:
            file = self.basis_filename
        else:
            file = filename 
            
        file = self.open_file(file)
        line_number= 0 
        for line in file:
            line_number += 1
            if line == keyword:
                line_number = line_number - 1
                break
            
        return line_number

    def divide_file(self, filename = None, keyword = None, line_number = None):
        
        if filename == None:
            file = self.basis_filename
            file = self.open_file(file)
        else:
            file = filename
        
        if line_number == None:
            line_number = self.find_keyword(keyword = keyword, filename = file)
        else:
            line_number = line_number
        
        before_keyword = file[0:line_number]
        after_keyword = file[line_number+1:len(file)]
        
        return before_keyword, after_keyword
        
    def list_combine(self, list_1, list_2, list_3):
        
        combined_list = list_1 + list_2 + list_3
        
        return combined_list