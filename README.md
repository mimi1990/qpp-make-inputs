## QPP-make-inputs
## Introduction
Imagine, that you have a lot of e.g. .xyz files and for each you want to perform identical calculation by e.g. Quantum ESPRESSO program. Probably you would patiently copy and paste each file to some clone of main file, or you just got an idea, that such type of job can be automated by Python script. Such Python script is already written in this repository. Furthermore, graphene example on how to use this repository is added. 

# Introduction

Python script **make_inputs.py** contain a class **MakeInputs** with useful methods for managing between one fixed core file and many files with variable content. Core file have constant input text data and marked keywords lines, files with variable content are fulfilled by text lines which can be insert int place of special keywords line in core file. 

This example shows one of the possible way of how to use this script. You will need compiled on your computer **Quantum ESPRESSO** and **Python** (I believe that there is no version limitation in both cases). For Python you should have 'pip-ed' **Numpy** and Matplotlib (optional, for visualization).

From scientific point of view, the example will be based on graphene and SCF plane wave calculations. The task is to find most stable distance between graphene sheets. In other words we have to find energy minimum in respect to **c** dimension.

# Step 0 - download data and programs
[example folder](https://bitbucket.org/mimi1990/qpp-make-inputs/src/master/make_inputs/examples/) contain lot of **gap_*.txt** files with **a**, **b**, **c** and  **cos(AB)** lines of Quantum ESPRESSO input - as you can see, only **c** is changing (not constantly - in the region of minimum change is 0.25&#8491; but in region of... dissociation (?) change is e.g. 4&#8491;). In the folder we can find also the core input file and running script.

Make sure that you have ../MakeInputs.py, every ./gap_*.txt file and ./make_a_lot_of_graphene_inputs.py in the same folder on your machine with compiled Quantum ESPRESSO (pw.x module).

# Step 1 - Run the make_a_lot_of_graphene_inputs.py
Script make_a_lot_of_graphene_inputs.py will:
1. Scan the folder ./ in searching \*.txt files - so it will find and save as a list of strings all ./gap_*.txt files
2. For each file script will find **PREFIX_keyword**, **GAP_keyword**. In first case PREFIX_keyword will be changed by one line of properly formatted text based on i'th element of _inputs_ list . Former case with GAP_keyword, this one line will be changed by gap_*.txt content - and this is what makes a difference between files.
3. Formatted core input will be saved in the same folder under the same name as gap_*.txt but with .in extension.

# Step 2 - Run gap_*.in calculations
I believe that every calculations can be easily done on local machine because they are not heavy. If in your ~/.bashrc you have defined path to Quantum ESPRESSO src folder then just type pw.x < gap_\*.in > gap\_*.out for every input file (or make bash script, then computer will do this for you). When SCF calculations will end you should make a list with respect to distance **c**, **!    total energy** and **the Fermi energy is** (you can also grep them out).

# Step 3 - Visualize it!
Now if you have data stored like in [this file](https://github.com/Miloszek1990/quantum-prepost/blob/master/make_inputs/examples/graphene_gap_A.txt) you can plot it easily by this [plot script](https://bitbucket.org/mimi1990/qpp-make-inputs/src/master/make_inputs/examples/plot.py). Thanks to such performed calculations you can see that the most stable distance between graphene sheets is around 9&#8491;. We can observe that, such calculations are very important because we can find e.g. the most stable distance configuration (minimum of Total energy).
![Image](https://bitbucket.org/mimi1990/qpp-make-inputs/downloads/TE_FE_sheet.png)

# Step 4 - Enjoy it and make science!
Thanks to this make_inputs.py script with MakeInput class you can test every parameter in your input. This is the easy tool for avoiding the **garbage in garbage out** sentence. I know that this example is very basic and only one parameter was tested, but accuracy was not the point of this example (btw. but accuracy is not so bad ;) )
